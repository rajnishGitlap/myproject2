trigger BatchApexErrorTrigger on BatchApexErrorEvent (after insert) 
{
    List<BatchLeadConvertErrors__c> CEList=new  List<BatchLeadConvertErrors__c>();
    BatchLeadConvertErrors__c BLCE;
    for(BatchApexErrorEvent BEE: Trigger.new)
    {
        BLCE=new BatchLeadConvertErrors__c();
        BLCE.AsyncApexJobId__c=BEE.AsyncApexJobId;
        BLCE.Records__c=BEE.JobScope;
        BLCE.StackTrace__c=BEE.StackTrace;
        CEList.add(BLCE);
    }
    if(CEList.size() > 0)
    {
        insert CEList;
    } 

}