/***************************************************************************************************************************************************************************
@Sequence : ITriggerHandler-->TriggerDispatcher-->AccountTriggerHandler--AccountTriggerUtility
@ Trigger  :      Account Trigger
@ Version:        1.0
@ Purpose:        Apex Trigger for task object.
@ discrication : The Trigger call TriggerDispatcher class and from there it will call corsponding class 

**************************************************************************************************************************************************************************/
trigger AccountTrigger on Account(before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerDispatcher.Run(new AccountTriggerHandler());
}