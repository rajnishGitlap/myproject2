({    
	navigateToMyComponent : function(component, event, helper) {

    var evt = $A.get("e.force:navigateToComponent");
    evt.setParams({
        componentDef : "c:DisplayOppt",
        componentAttributes: {
            "accountRecordId":component.get("v.recordId")

        }
    });
    evt.fire();
}
})