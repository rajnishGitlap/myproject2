({
    doInit : function(component, event, helper)

{
var pageSize = component.get("v.pageSize");
alert('pageSize=='+pageSize);
var action = component.get("c.getAllOpportunities");
action.setCallback(this, function(response){
var state = response.getState();
if (component.isValid() && state === "SUCCESS"){
component.set("v.opportunityList", response.getReturnValue());
component.set("v.totalSize", component.get("v.opportunityList").length);
component.set("v.start",0);
component.set("v.end",pageSize-1);
var paginationList = [];
for(var i=0; i< pageSize; i++){
paginationList.push(response.getReturnValue()[i]);
}

component.set("v.paginationList", paginationList);
}

});

$A.enqueueAction(action);

},
onSelectChange : function(component, event, helper) {
var selected = component.find("records").get("v.value");
var paginationList = [];
var oppList = component.get("v.opportunityList");
for(var i=0; i< selected; i++){
//paginationList.push(response.getReturnValue()[i]);
paginationList.push(oppList[i]);

}
component.set("v.paginationList", paginationList);

}
})