({
doInit : function(component, event, helper)
{

var action = component.get("c.getOpportunities");
action.setParams({
        AccountId : component.get("v.accountRecordId")
    });
action.setCallback(this, function(response){
var state = response.getState();
if (component.isValid() && state === "SUCCESS"){
component.set("v.opportunityList", response.getReturnValue());
}
});

$A.enqueueAction(action);

},
 handleChange : function(component, event, helper)
    {       
        var oppid=event.getSource().get("v.text");       
        component.set('v.opptunityId',oppid);        
       
    },
  searchKeyChange: function(component, event, helper)
    {
        
        var searchKey =  component.find("input1").get("v.value");
        var AccountId=component.get("v.accountRecordId");
        
        var action = component.get("c.getByName");
        action.setParams({
			"searchKey": searchKey,
            "AccountId" : AccountId

		});
        action.setCallback(this, function(response) {
          var state = response.getState();
          if (component.isValid() && state === "SUCCESS"){
            component.set("v.opportunityList", response.getReturnValue());
            }
        });

$A.enqueueAction(action);

},

})