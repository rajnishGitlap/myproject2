/**************************************************************************************************************************************************************************
@Sequence : ITriggerHandler-->TriggerDispatcher-->AccountTriggerHandler--AccountTriggerUtility 
@ Class  :       AccountTriggerHandler
@ Author :       Rajnish
@ Purpose:       Apex Class to handle events on Account and call specific methods from Task trigger utility class.

***************************************************************************************************************************************************************************/
public without sharing class AccountTriggerHandler implements ITriggerHandler
{   
    public static Boolean AccountTriggerDisabled = false;
   
    public Boolean IsDisabled()
    {
        if (TriggerSettings__c.getInstance().AccountTriggerDisabled__c){
            return true;
        }
        else{
            return AccountTriggerDisabled;
        }
    }
    
    public void BeforeInsert(List<SObject> newItems)
    { 
        List<Account> accList = (List<Account>)newItems;
       // beforeInsertFunctionalities.beforeInsertFunctionalities (taskList);   
        
        for(Account acc: accList ){
           // dummyclass.dummyfuncation(acc);                
            }        
    }
    
     public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems)
     {
        List<Account> accListvalue = (List<Account>)newItems.values();
        Map<Id,Account> oldMap = (Map<Id, Account>)oldItems;
        Map<Id,Account> newMap = (Map<Id, Account>)newItems;
        List<Account> accList=new List<Account>();
     
       // Id PEMTaskRECId=RecordTypeUtil.getAPSTaskRTByName(RecordTypeUtil.PEM_TASK);
    
          for(Account acc: accListvalue){     
           // DummayClass.Dummyfucation(tsk);
          //  DummayClass1.Dummyfucation1(tsk,oldMap); 
            }
           
       // DummayClass3.Dummyfucation3(accList,oldMap);
       
    }
    
    public void BeforeDelete(Map<Id, SObject> oldItems)
    { 
       List<Account> accListvalue = (List<Account>)oldItems.values();
       // Implement the code       
    }
    
    public void AfterInsert(Map<Id, SObject> newItems)
    { 
        List<Account> accListvalue = (List<Account>)newItems.values();
       // Implement the code       
    }
    
     public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems)
     {
        List<Account> accListvalue = (List<Account>)newItems.values();
        Map<Id,Account> oldMap = (Map<Id, Account>)oldItems;
        Map<Id,Account> newMap = (Map<Id, Account>)newItems;
        List<Account> accList=new List<Account>();
     
       // Id PEMTaskRECId=RecordTypeUtil.getAPSTaskRTByName(RecordTypeUtil.PEM_TASK);
    
          for(Account acc: accListvalue){     
           // DummayClass.Dummyfucation(tsk);
          //  DummayClass1.Dummyfucation1(tsk,oldMap); 
            }
           
       // DummayClass3.Dummyfucation3(accList,oldMap);
       
    }
    
    public void AfterDelete(Map<Id, SObject> oldItems)
    { 
       // Implement the code       
    }
    
      public void AfterUndelete(Map<Id, SObject> oldItems)
    { 
       // Implement the code       
    }
 }