/***************************************************************************************************************************************************************************
@Sequence : ITriggerHandler-->TriggerDispatcher-->AccountTriggerHandler--AccountTriggerUtility
@ Interface  :    ITriggerHandler
@ Version:        1.0
@ Author :        Rajnish
@ Purpose:        Apex Interface for the Add our all logics implement in ITriggerHandler.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@ Change history: 04.Aug.2020/
***************************************************************************************************************************************************************************/
public interface ITriggerHandler {
    void BeforeInsert(List<SObject> newItems);
    void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems);
    void BeforeDelete(Map<Id, SObject> oldItems);
    void AfterInsert(Map<Id, SObject> newItems);
    void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems);
    void AfterDelete(Map<Id, SObject> oldItems);
    void AfterUndelete(Map<Id, SObject> oldItems);
    Boolean IsDisabled();
}