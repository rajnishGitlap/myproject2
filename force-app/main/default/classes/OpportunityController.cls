public class OpportunityController
{

@AuraEnabled
public static List<String> getselectOptions(sObject objObject, string fld)
{
  List < String > allOpts = new list < String > ();
  
  // Get the object type of the SObject.
  Schema.sObjectType objType = objObject.getSObjectType();
 
  // Describe the SObject using its object type.
  Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
 
  // Get a map of fields for the SObject
  map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
 
  // Get the list of picklist values for this field.
  list <Schema.PicklistEntry > values =
   fieldMap.get(fld).getDescribe().getPickListValues();
 system.debug('');
  // Add these values to the selectoption list.
  for (Schema.PicklistEntry a: values) {
   allOpts.add(a.getValue());
}
    system.debug('allOpts ---->' + allOpts);
  allOpts.sort();
  return allOpts;
}
@AuraEnabled
public static List<Account> fetchAccounts()
{
            List<Account> accList = [SELECT Id, Name, BillingState, 
                                        Website, Phone, Industry, Type from Account];
            return accList;
}
@AuraEnabled
public static List<Opportunity> getAllOpportunities()
{
  return [SELECT Id, Name,StageName,CloseDate,Amount,CurrentGenerators__c,MainCompetitors__c,OrderNumber__c,Probability,TrackingNumber__c FROM Opportunity Where Amount!=null];
}    

@AuraEnabled
public static integer OpportunityCount()
{
	return [SELECT count() FROM Opportunity Where Amount!= Null];
}

// This method used for reterived  the list of opportunity records based on the search string

@AuraEnabled
public static List<Opportunity> getByName(String searchKey,string AccountId) {
String name = '%' + searchKey + '%';
return [SELECT id, Name,StageName, CloseDate,Amount,CurrentGenerators__c,MainCompetitors__c,OrderNumber__c,Probability,TrackingNumber__c FROM Opportunity WHERE name LIKE :name and AccountId=:AccountId];

}
}