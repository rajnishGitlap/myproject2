global class TestBatchClass implements Database.Batchable<sObject>,Database.Stateful{
    
    global List<string> lstcountid=new List<string>();
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // collect the batches of records or objects to be passed to execute
         
       // String query = 'Select count(Id) from Call2_Detail_vod__c where mck_core_Country__c=FR';
	   String query = 'Select id from account limit 580';
	   return Database.getQueryLocator(query);
    }
     
    global void execute(Database.BatchableContext BC, List<Account> accList) {
        
        system.debug('accList.size()=='+accList.size());
        // process each batch of records default size is 200
        for(Account cc : accList) {          
		  
		    lstcountid.add(cc.id);
        }
         
        
         
    }   
     
    global void finish(Database.BatchableContext BC) {
        
        system.debug('lstcountid.size()=27='+lstcountid.size());
        List<String> toAddresses = new List<String>();
        toAddresses.add('r.ab.kumar.singh@accenture.com');
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           //String[] toAddresses = new String[] {a.CreatedBy.Email};
          // String toAddresses ='rajnish.k.singh@accenture.com';
            mail.setToAddresses(toAddresses);
            mail.setSubject('Match Merge Batch ' + lstcountid.size());
            mail.setPlainTextBody('records processed ' + lstcountid.size());
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

}