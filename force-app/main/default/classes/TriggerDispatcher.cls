/*
-----------------------------------------------------------------------------------------------------------------
@Sequence : ITriggerHandler-->TriggerDispatcher-->AccountTriggerHandler--AccountTriggerUtility
 @ Purpose:Apex Class for the Add our all logics implement in TriggerDispatcher.
Call this method from your trigger, passing in an instance of a trigger handler which implements ITriggerHandler.
This method will fire the appropriate methods on the handler depending on the trigger context.
********************************************************************************************************************
*/

public class TriggerDispatcher {

    public static void Run(ITriggerHandler handler)
    {
        try{
            
            if (handler.IsDisabled())
                return;
            if (Trigger.IsBefore )
            {
                if (Trigger.IsInsert){
                    handler.BeforeInsert(trigger.new);
                }
                if (Trigger.IsUpdate){
                    handler.BeforeUpdate(trigger.newMap, trigger.oldMap);
                }   
                if (Trigger.IsDelete){
                    handler.BeforeDelete(trigger.oldMap);
                }
            }
            if (Trigger.IsAfter)
            {
                if (Trigger.IsInsert){
                    handler.AfterInsert(Trigger.newMap);
                }
                if (Trigger.IsUpdate){
                    handler.AfterUpdate(trigger.newMap, trigger.oldMap);
                }
                if (trigger.IsDelete){
                    handler.AfterDelete(trigger.oldMap);
                }
                if (trigger.isUndelete){
                    handler.AfterUndelete(trigger.oldMap);
                }
            }
        }
        
        catch(Exception e)
        {
            //errorHandlingUtilityClass.handleException(e,'TriggerDispatcher','Run'); 
        }
    }  
    
}